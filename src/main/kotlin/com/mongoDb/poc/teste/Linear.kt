package com.mongoDb.poc.teste

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.LocalDateTime

@Document
data class Linear(
    @Id
    val id: ObjectId = ObjectId.get(),
    val createdDate: LocalDateTime = LocalDateTime.now(),
    val countyType: CountyType,
    val distributionCenterCode: String,
    val skuRepresentation: SkuRepresentation,
    val curveCode: String,
    val stockRepresentation: StockRepresentation,
    val countryPendingQuantity: Long,
    val dailyPendingQuantity: Double,
    val fillRate: BigDecimal,
    val testado: Boolean
)