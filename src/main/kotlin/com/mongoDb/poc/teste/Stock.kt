package com.mongoDb.poc.teste

data class Stock(
    val stockInDistributionCenter: Long?,
    val stockInQuality: Int,
    val stockInTransit: Int,
    val totalStockAllocation: Long,
    val totalStockInQuality: Long?,
    val totalStockInTransit: Long?
)