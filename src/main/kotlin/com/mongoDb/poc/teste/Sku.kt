package com.mongoDb.poc.teste

data class Sku(
    val code: Int,
    val description: String
)