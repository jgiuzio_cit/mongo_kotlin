package com.mongoDb.poc.teste

import com.mongoDb.poc.teste.Linear
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface LinearRepository : MongoRepository<Linear, String> {
    fun findOneById(id: ObjectId): Linear
    override fun deleteAll()

}