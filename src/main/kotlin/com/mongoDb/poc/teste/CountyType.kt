package com.mongoDb.poc.teste

enum class CountyType(val type: String) {
    CAPITAL("Capital"),
    CAPITAL_INTERIOR("Capital/Interior"),
    INTERIOR("Interior");

    companion object {

        fun fromCountyCode(value: String): CountyType {
            val leftZeroRegexRemoval = "^0+(?!$)".toRegex()

            return when (value.replaceFirst(leftZeroRegexRemoval, "")) {
                "1", "5" -> CAPITAL
                "2", "6" -> INTERIOR
                "" -> CAPITAL_INTERIOR
                else -> throw IllegalArgumentException("Invalid county code.")
            }
        }

        fun fromCountyType(value: CountyType): String {
            return when (value) {
                CAPITAL -> "1"
                INTERIOR -> "2"
                CAPITAL_INTERIOR -> ""
            }
        }
    }
}
