package com.mongoDb.poc.teste

import kotlin.random.Random

data class SkuRepresentation(
    val code: String,
    val description: String
) {
    constructor(sku: Sku) : this(
        code = Random(sku.code).nextInt().toString(),
        description =  "Teste ${Random(1).nextInt()} - ${sku.description}"
    )
}