package com.mongoDb.poc

import com.mongoDb.poc.teste.*
import org.bson.types.ObjectId
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal
import java.time.LocalDateTime
import kotlin.random.Random

@RestController
@RequestMapping("/patients")
class PatientController(
    private val patientsRepository: PatientRepository,
    private val linearRepository: LinearRepository
) {
    @GetMapping
    fun getAllPatients(): ResponseEntity<List<Patient>> {
        val patients = patientsRepository.findAll()
        return ResponseEntity.ok(patients)
    }

    @GetMapping("/{id}")
    fun getOnePatient(@PathVariable("id") id: String): ResponseEntity<Patient> {
        val patient = patientsRepository.findOneById(ObjectId(id))
        return ResponseEntity.ok(patient)
    }

    @PostMapping()
    fun createPatient(@RequestBody request: PatientRequest): ResponseEntity<Patient> {
        val patient = patientsRepository.save(
            Patient(
                name = request.name,
                description = request.description
            )
        )
        return ResponseEntity(patient, HttpStatus.CREATED)
    }

    @GetMapping("/teste")
    fun getTeste(): ResponseEntity<Linear> {

        val linear = linearRepository.save(
            Linear(
                id = ObjectId.get(),
                createdDate = LocalDateTime.now(),
                countyType = CountyType.CAPITAL,
                distributionCenterCode = "DB01",
                skuRepresentation = SkuRepresentation(Sku(code = Random(10).nextInt(), "Teste")),
                curveCode = "L",
                stockRepresentation = StockRepresentation(
                    stockInDistributionCenter = null,
                    stockInQuality = 1,
                    stockInTransit = 1231231231,
                    totalStockAllocation = 125L,
                    totalStockInQuality = 25L,
                    totalStockInTransit = 1025L
                ),
                countryPendingQuantity = 1000L,
                dailyPendingQuantity = 50.10,
                fillRate = BigDecimal(10.56),
                testado = true
            )
        )
        val retorno = linearRepository.findOneById(linear.id)
        return ResponseEntity.ok(retorno)
    }

}