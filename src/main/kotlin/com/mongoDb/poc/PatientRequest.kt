package com.mongoDb.poc

data class PatientRequest (
    val name: String,
    val description: String
)