# README #

### What is this repository for? ###

* Aprender Kotlin com Mongo e RabbitMQ

### How do I get set up? ###

* Ter a JDK 11;
* Instalar docker-compose e rodar o docker-compose presente em srs/main/resources;
* Instalar o Compass;
* Acessar o banco na url presente no aplication.properties com o compass;
* Pronto para rodar a aplicação e testar.
